angular.module('starter.services', [])

.factory('Chats', function() {
 // Might use a resource here that returns a JSON array

 // Some fake testing data
 var chats = [
{
   id: 0,
   name: 'Kilian Dbs',
   lastText: 'You on your way?',
   face: 'img/ben.png',
   email: 'kiliandubois@hotmail.fr',
   phone: '07 81 55 38 67',
   city: 'Saint-Amand-Les-Eaux',
   job: 'Developpeur Web'
 }, {
   id: 1,
   name: 'Younes Labachi',
   lastText: 'Hey, it\'s me',
   face: 'img/max.png',
   email: 'labachi.younes@hotmail.fr',
   phone: 'Non joignable',
   city: 'Denain',
   job: 'Developpeur Web'
 }, {
   id: 2,
   name: 'Pierre Buirette',
   lastText: 'I should buy a boat',
   face: 'img/adam.jpg',
   email: 'buirette.pierre@gmail.com',
   phone: '06 80 51 80 92',
   city: 'Denain',
   job: 'Web Designer'
 }, {
   id: 3,
   name: 'Perry Governor',
   lastText: 'Look at my mukluks!',
   face: 'img/perry.png',
   email: 'perry.ny@orange.fr',
   phone: '555-0108',
   city: 'New York',
   job: 'Cryptographe'
 }, {
   id: 4,
   name: 'Mike Harrington',
   lastText: 'This is wicked good ice cream.',
   face: 'img/mike.png',
   email: 'Non joignable',
   phone: 'Non joignable',
   city: 'Nouveau Mexique',
   job: 'Hacker'
 }];

 return {
   all: function() {
     return chats;
   },
   remove: function(chat) {
     chats.splice(chats.indexOf(chat), 1);
   },
   get: function(chatId) {
     for (var i = 0; i < chats.length; i++) {
       if (chats[i].id === parseInt(chatId)) {
         return chats[i];
       }
     }
     return null;
   }
 };
})







// Fonction pour récupérer les veilles
.factory('VeillesFactory', function($http){
 return {
     // Toutes les veilles
   getAll: function() {
     return $http.get('http://veille.popschool.fr/api/?api=veille&action=getAll');
   },
     // Veille par ID
   get: function(veilleId) {
     return $http.get('http://veille.popschool.fr/api/?api=veille&action=get&id='+veilleId);
   }
 }
})

// Fonction pour récupérer les utilisateurs
.factory('UsersFactory', function($http){
 return {
     // Tous les utilisateurs
   getAll: function() {
     return $http.get('http://veille.popschool.fr/api/?api=user&action=getAll');
   },
     // Utilisateur par ID
   get: function(userId) {
     return $http.get('http://veille.popschool.fr/api/?api=user&action=get&id='+userId);
   },
   getByUser:function (userId) {
      return $http.get('http://veille.popschool.fr/api/?api=veille&action=getByUser&id_user=' + userId);
    },
    auth:function(email, password) {
  return $http.get('http://veille.popschool.fr/api/?api=user&action=auth&email='+email+'&password='+password);
  }
 }
});
