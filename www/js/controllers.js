angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
 // With the new view caching in Ionic, Controllers are only called
 // when they are recreated or on app start, instead of every page change.
 // To listen for when this page is active (for example, to refresh data),
 // listen for the $ionicView.enter event:
 //
 //$scope.$on('$ionicView.enter', function(e) {
 //});

 $scope.chats = Chats.all();
 $scope.remove = function(chat) {
   Chats.remove(chat);
 };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
 $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
 $scope.settings = {
   enableFriends: true
 };
})


// Toutes les veilles
.controller('VeillesCtrl', function($scope, VeillesFactory){
  $scope.veilles = [];
  VeillesFactory.getAll().then(function(recup){
    $scope.veilles = recup.data.veilles;
  });
})

// Veille par ID
.controller('VeilleDetailCtrl', function($scope, $stateParams, VeillesFactory) {
  VeillesFactory.get($stateParams.veilleId).then(function(recup){
    $scope.veille = recup.data.veille;
    //console.log($scope.veille);
  });
  //console.log($stateParams.veilleId)
})
// Tous les utilisateurs
.controller('UsersCtrl', function($scope, UsersFactory){
  $scope.users = [];
  UsersFactory.getAll().then(function(recup){
    $scope.users = recup.data.users;
  });
})


// User par ID
.controller('UserDetailCtrl', function($scope, $stateParams, UsersFactory) {
  UsersFactory.getByUser($stateParams.userId).then(function(recup){
    $scope.veilles = recup.data.veilles;
    //console.log($scope.veilles);
  });
  UsersFactory.get($stateParams.userId).then(function(recup){
    //console.log(r.data.user[0]);
    $scope.user = recup.data.user[0];
    //console.log($scope.veille);
  });
  //console.log($stateParams.veilleId)
})
/////teste local storage----------------------------------------------------
// function auth(){
//   if UserFactory.auth($scope.email, $scope.password)
// }
// auth : function(email,password){
// var return=$http.get("http://veille.popschool.fr/connexion.php");
// if (returen.data.status == 'ok')}
// session Storage.email=email;
// return true;{
//   return false
// }
.controller ('LogCtrl', function ($scope, UsersFactory) {
  $scope.data = {};

  $scope.login = function() {
    console.log($scope.data.password, $scope.data.email);

    UsersFactory.auth($scope.data.email,$scope.data.password).then(function(t){
      if(t.data.status=="ok"){
        console.log("ok");
        return true;
      }
      else{
        console.log("no");

        return false;
      }
    });
  }
})

.controller('AccountCtrl', function($scope){
  $scope.settings = {
    enableFriends: true
  };
});
